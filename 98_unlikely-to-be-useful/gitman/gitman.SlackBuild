#!/bin/bash

# Based on Copyright 2019, Patrick Volkerding
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Used in 2019 by Lockywolf gmail.com

cd $(dirname $0) ; CWD=$(pwd)

PKGNAM=git
VERSION=${VERSION:-$(echo $PKGNAM-*.tar.?z* | cut -d - -f 2 | rev | cut -f 3- -d . | rev)}
BUILD=${BUILD:-1}


# If the variable PRINT_PACKAGE_NAME is set, then this script will report what
# the name of the created package would be, and then exit. This information
# could be useful to other scripts.
if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PKGNAM-$VERSION-$ARCH-$BUILD.txz"
  exit 0
fi


TMP=${TMP:-/tmp}
PKG=$TMP/package-gitman

rm -rf $PKG
mkdir -p $TMP $PKG

cd $TMP
rm -rf git-$VERSION
tar xvf $CWD/git-$VERSION.tar.?z* || exit 1
cd git-$VERSION || exit 1

chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

mkdir -p $PKG/usr/info/
cd Documentation || exit 1
make git.info gitman.info
cp git.info $PKG/usr/info/
cp gitman.info $PKG/usr/info/


mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat > $PKG/install/doinst.sh <<EOF
cd /usr/info/ || exit 1
install-info git.info dir
install-info gitman.info dir
EOF


cd $PKG || exit 1
ARCH=noarch
/sbin/makepkg -l y -c n $TMP/gitman-$VERSION-$ARCH-$BUILD.txz

