diff --git a/libdino/src/entity/settings.vala b/libdino/src/entity/settings.vala
index 97ea5482..fa19b415 100644
--- a/libdino/src/entity/settings.vala
+++ b/libdino/src/entity/settings.vala
@@ -12,6 +12,7 @@ public class Settings : Object {
         notifications_ = col_to_bool_or_default("notifications", true);
         convert_utf8_smileys_ = col_to_bool_or_default("convert_utf8_smileys", true);
         check_spelling = col_to_bool_or_default("check_spelling", true);
+        trayicon = col_to_bool_or_default("trayicon", true);
     }
 
     private bool col_to_bool_or_default(string key, bool def) {
@@ -19,6 +20,18 @@ public class Settings : Object {
         return val != null ? bool.parse(val) : def;
     }
 
+    private bool trayicon_;
+    public bool trayicon {
+        get { return trayicon_; }
+        set {
+            db.settings.upsert()
+                    .value(db.settings.key, "trayicon", true)
+                    .value(db.settings.value, value.to_string())
+                    .perform();
+            trayicon_ = value;
+        }
+    }
+
     private bool send_typing_;
     public bool send_typing {
         get { return send_typing_; }
diff --git a/main/data/settings_dialog.ui b/main/data/settings_dialog.ui
index d5b7ac92..5956c46d 100644
--- a/main/data/settings_dialog.ui
+++ b/main/data/settings_dialog.ui
@@ -77,6 +77,18 @@
                                 <property name="height">1</property>
                             </packing>
                         </child>
+                        <child>
+                            <object class="GtkCheckButton" id="trayicon_checkbutton">
+                                <property name="label" translatable="yes">Tray icon</property>
+                                <property name="visible">True</property>
+                            </object>
+                            <packing>
+                                <property name="left_attach">0</property>
+                                <property name="top_attach">5</property>
+                                <property name="width">1</property>
+                                <property name="height">1</property>
+                            </packing>
+                        </child>
                     </object>
                 </child>
             </object>
diff --git a/main/src/main.vala b/main/src/main.vala
index afa1f52b..f90e6655 100644
--- a/main/src/main.vala
+++ b/main/src/main.vala
@@ -1,9 +1,12 @@
+using Gtk;
 using Dino.Entities;
 using Dino.Ui;
 
 extern const string GETTEXT_PACKAGE;
 extern const string LOCALE_INSTALL_DIR;
 
+StatusIcon trayicon;
+
 namespace Dino {
 
 void main(string[] args) {
diff --git a/main/src/ui/application.vala b/main/src/ui/application.vala
index bed6d01b..5015bf69 100644
--- a/main/src/ui/application.vala
+++ b/main/src/ui/application.vala
@@ -11,6 +11,45 @@ public class Dino.Ui.Application : Gtk.Application, Dino.Application {
     private const string[] KEY_COMBINATION_LOOP_CONVERSATIONS = {"<Ctrl>Tab", null};
     private const string[] KEY_COMBINATION_LOOP_CONVERSATIONS_REV = {"<Ctrl><Shift>Tab", null};
 
+    private Gtk.Menu menuSystem;
+    public bool mactive = false;
+    private void menuSystem_popup(uint button, uint time) {
+        mactive = window.is_active;
+        menuSystem.popup(null, null, null, button, time);
+    }
+
+    private void tray_clicked_left() {
+        tray_clicked(false);
+    }
+    private void tray_clicked_menu() {
+        tray_clicked(true);
+    }
+
+    private void tray_clicked(bool from_menu) {
+        if (window == null) {
+            controller = new MainWindowController(this, stream_interactor, db);
+            config = new Config(db);
+            window = new MainWindow(this, stream_interactor, db, config);
+            controller.set_window(window);
+            window.present();
+        }
+        window.delete_event.connect((event) => {
+            window.hide();
+            return true;
+        });
+        if(from_menu==false) mactive = window.is_active;
+        if(window.visible==false) {
+            window.show();
+        } else {
+            if(mactive==false) {
+                window.present();
+            } else {
+                window.hide();
+            }
+        }
+    }
+
+
     private MainWindow window;
     public MainWindowController controller;
 
@@ -39,6 +78,55 @@ public class Dino.Ui.Application : Gtk.Application, Dino.Application {
 
         create_actions();
         add_main_option_entries(options);
+        if(settings.trayicon) {
+            trayicon = new StatusIcon.from_icon_name("im.dino.Dino");
+            trayicon.set_tooltip_text ("Dino");
+            trayicon.set_visible(true);
+            trayicon.activate.connect(tray_clicked_left);
+
+            menuSystem = new Gtk.Menu();
+
+            var box = new Box (Orientation.HORIZONTAL, 6);
+            var label = new Label ("Show/Hide Dino");
+            var menuItem = new Gtk.MenuItem();
+            box.add (label);
+            menuItem.add (box);
+            menuItem.activate.connect(tray_clicked_menu);
+            menuSystem.append(menuItem);
+
+            box = new Box (Orientation.HORIZONTAL, 6);
+            label = new Label ("Accounts");
+            menuItem = new Gtk.MenuItem();
+            box.add (label);
+            menuItem.add (box);
+            menuItem.activate.connect(show_accounts_window);
+            menuSystem.append(menuItem);
+            box = new Box (Orientation.HORIZONTAL, 6);
+            label = new Label ("Settings");
+            menuItem = new Gtk.MenuItem();
+            box.add (label);
+            menuItem.add (box);
+            menuItem.activate.connect(show_settings_window);
+            menuSystem.append(menuItem);
+
+            box = new Box (Orientation.HORIZONTAL, 6);
+            label = new Label ("About");
+            menuItem = new Gtk.MenuItem();
+            box.add (label);
+            menuItem.add (box);
+            menuItem.activate.connect(show_about_window);
+            menuSystem.append(menuItem);
+
+            box = new Box (Orientation.HORIZONTAL, 6);
+            label = new Label ("Quit");
+            menuItem = new Gtk.MenuItem();
+            box.add (label);
+            menuItem.add (box);
+            menuItem.activate.connect(quit);
+            menuSystem.append(menuItem);
+            menuSystem.show_all();
+            trayicon.popup_menu.connect(menuSystem_popup);
+        }
 
         startup.connect(() => {
             if (print_version) {
@@ -53,6 +141,7 @@ public class Dino.Ui.Application : Gtk.Application, Dino.Application {
                 notification_events.register_notification_provider(free_desktop_notifier);
             }
             notification_events.notify_content_item.connect((content_item, conversation) => {
+                trayicon.set_from_icon_name("dino-emoticon-symbolic");
                 // Set urgency hint also if (normal) notifications are disabled
                 // Don't set urgency hint in GNOME, produces "Window is active" notification
                 var desktop_env = Environment.get_variable("XDG_CURRENT_DESKTOP");
@@ -70,7 +159,10 @@ public class Dino.Ui.Application : Gtk.Application, Dino.Application {
                 config = new Config(db);
                 window = new MainWindow(this, stream_interactor, db, config);
                 controller.set_window(window);
-                if ((get_flags() & ApplicationFlags.IS_SERVICE) == ApplicationFlags.IS_SERVICE) window.delete_event.connect(window.hide_on_delete);
+                if ((get_flags() & ApplicationFlags.IS_SERVICE) == ApplicationFlags.IS_SERVICE || settings.trayicon) window.delete_event.connect((event) => {
+                    window.hide();
+                    return true;
+                });
             }
             window.present();
         });
diff --git a/main/src/ui/main_window_controller.vala b/main/src/ui/main_window_controller.vala
index dceb4094..1c3ba6d2 100644
--- a/main/src/ui/main_window_controller.vala
+++ b/main/src/ui/main_window_controller.vala
@@ -72,6 +72,7 @@ public class MainWindowController : Object {
             return false;
         });
         window.focus_in_event.connect(() => {
+            trayicon.set_from_icon_name("im.dino.Dino");
             stream_interactor.get_module(ChatInteraction.IDENTITY).on_window_focus_in(conversation);
             window.urgency_hint = false;
             return false;
diff --git a/main/src/ui/settings_dialog.vala b/main/src/ui/settings_dialog.vala
index e994e00c..4dbf90f5 100644
--- a/main/src/ui/settings_dialog.vala
+++ b/main/src/ui/settings_dialog.vala
@@ -10,6 +10,7 @@ class SettingsDialog : Dialog {
     [GtkChild] private unowned CheckButton notification_checkbutton;
     [GtkChild] private unowned CheckButton emoji_checkbutton;
     [GtkChild] private unowned CheckButton check_spelling_checkbutton;
+    [GtkChild] private CheckButton trayicon_checkbutton;
 
     Dino.Entities.Settings settings = Dino.Application.get_default().settings;
 
@@ -21,12 +22,14 @@ class SettingsDialog : Dialog {
         notification_checkbutton.active = settings.notifications;
         emoji_checkbutton.active = settings.convert_utf8_smileys;
         check_spelling_checkbutton.active = settings.check_spelling;
+        trayicon_checkbutton.active = settings.trayicon;
 
         typing_checkbutton.toggled.connect(() => { settings.send_typing = typing_checkbutton.active; } );
         marker_checkbutton.toggled.connect(() => { settings.send_marker = marker_checkbutton.active; } );
         notification_checkbutton.toggled.connect(() => { settings.notifications = notification_checkbutton.active; } );
         emoji_checkbutton.toggled.connect(() => { settings.convert_utf8_smileys = emoji_checkbutton.active; });
         check_spelling_checkbutton.toggled.connect(() => { settings.check_spelling = check_spelling_checkbutton.active; });
+        trayicon_checkbutton.toggled.connect(() => { settings.trayicon = trayicon_checkbutton.active; });
     }
 }
 
