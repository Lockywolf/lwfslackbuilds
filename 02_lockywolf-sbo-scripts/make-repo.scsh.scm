#!/usr/bin/env -S scsh -dm -m sbo-main -e main -s
!#
;;; Time-stamp: <2022-08-12 13:38:54 lockywolf>
;;;#+Updated: <2022-06-13 Mon> : make it generate the first working thing.
;;;#+Author: lockywolf
;;;#+category: admin
;;;#+tags: linux, scheme, slackware, slackbuilds, scsh, heuristics

(define-structures ((sbo-main (export main))
                    (sbo-aux (export lwf_debug)))
  (open
   (subset define-record-types (define-record-type define-record-discloser))
   (subset records (record-type))
   (subset record-types (record-type-field-names record-accessor))
   finite-types
   (subset tables (make-table table-ref table-set!))
   scheme-with-scsh
   (subset exceptions (with-exception-handler))
   srfi-62)
  (begin
    (define lwf_debug #f)
    (define-syntax when-debug
      (syntax-rules ()
        ((_ . a)
         (if lwf_debug
            (begin
              . a)))))
    (define-syntax when
      (syntax-rules ()
        ((_ c . a)
         (if c
            (begin
              . a)))))
    ;;>> Config stuff. Eventually move it to a file in ~/.config, or smth.
    ;;> Where to download files.
    (define lwf_downloads_dir (expand-file-name "~/DevLinux/lwf_downloads_for_slackbuilds"))
    ;;> Downloader wrapper.
    (define wgexec (expand-file-name "~/bin/wgexec"))
    ;;> Where SBo templates are.
    (define lwf_sbo_templates_dir (expand-file-name "~/OfficialRepos/slackbuilds-templates"))
    ;;> what to place as a maintainer into the template.info
    (define lwf-maintainer "anonymous")
    (define lwf-maintainer-email "anonymous@legion.inc")

    (define (lwf-universal-record-printer rec)
      (list 'universal-printer
      (display "(") (display (record-type rec))
      (map (lambda (x)
             (newline)
             (display "(")
             (display x)
             (display " ")
             (display ((record-accessor (record-type rec) x) rec))
             (display ")"))
           (record-type-field-names (record-type rec)))
      (display ")")))


    (define-record-type :slackbuild-meta
      (make-slackbuild-meta commit-or-version-type
                            slackbuild-name
                            commit-or-version-string
                            homepage
                            url
                            md5)
      slackbuild-meta?
      (commit-or-version-type sbo:commit-or-version-type-get)
      (commit-or-version-string slackbuild-meta-commit-or-version-string-get)
      (slackbuild-name slackbuild-meta-slackbuild-name-get)
      (homepage sbo:homepage-get)
      (url sbo:url-get)
      (md5 sbo:md5-get)
      )

    (define-record-discloser :slackbuild-meta
      lwf-universal-record-printer)


    ;; (define-record-discloser :slackbuild-meta
    ;;   (lambda (x) `(slackbuild-meta
    ;;            ,(slackbuild-meta-slackbuild-name-get x)
    ;;            ,(slackbuild-meta-commit-or-version-type-get x)
    ;;            ,(slackbuild-meta-commit-or-version-string-get x))))

    (define (call-with-temporary-directory thunk)
      (define temporary-name
        (temp-file-iterate
         (lambda (y) (let ((x (resolve-file-name y (cwd))))
                  (if (file-exists? x) #f (if (create-directory x) x #f)))) "~a"))
      (define old-cwd (cwd))
      (define (before) (chdir temporary-name))
      (define (after) (run (rm -rf ,temporary-name)) (chdir old-cwd))
      (dynamic-wind before thunk after))

    (define-syntax with-temporary-directory
      (syntax-rules ()
        ((_ . a)
         (call-with-temporary-directory
          (lambda () . a)))))

    (define-syntax with-temporary-unpacked-archive
      (syntax-rules ()
        ((_ archive-full-path . exprs)
         (with-temporary-directory
          (run (tar xvf ,archive-full-path) (> "/dev/null"))
          (chdir (lwf_last_dir))
          .
          exprs))))

    (define (lwf_last_proto type)
      (car (run/strings
            (| ;;|
             (find "." -maxdepth 1 -type ,type -printf "%C@ - %p\n")
             (sort -n  )
             (tail -n 1)
             (awk "{printf $3;}")
             ))))
    (define (lwf_last_file)
      (lwf_last_proto "f"))
    (define (lwf_last_dir)
      (lwf_last_proto "d"))

    (define (lwf_sbo_fetch_source url)
      (with-cwd lwf_downloads_dir
                ;;(run (ls))
                (if (not lwf_debug) (run (,wgexec curl --remote-name ,url)))
                (lwf_last_file)))

    (define (lwf_sbo_detect_commit_or_version file-name url md5)
      (define (detect-homepage url name)
        "heuristic to extract the homepage from a url"
        (when #f (display "starting to detect homepage") (newline))
        (cond ((regexp-search
                (rx (seq (submatch (* any)) ,name (* any)))
                url)
              => (lambda (x) ;;(display "debug: matched=") (display (match:substring x 1)) (newline)
                    (match:substring x 1)))
              (else (error "Please add more heuristics to detect url!"))))
      (when #f (display "debug:detect_commit") (newline))
      (let ((file-name (simplify-file-name file-name)))
        (receive (package version type)
            (cond ((regexp-search
                    (rx (seq (submatch (* any)) "-" (submatch (= 40 (or ("01234567890abcdef"))))))
                    file-name)
                   => (lambda (x) (values (match:substring x 1) (match:substring x 2) 'commit)))
                  ((regexp-search
                    (rx (seq (submatch (* any)) "-" (submatch
                                                     (seq (or ("01234567890"))
                                                          (* (or ("01234567890.")))
                                                          (or ("01234567890"))
                                                          ))))
                    file-name) ;; todo: improve heuristic
                   => (lambda (x) (values (match:substring x 1) (match:substring x 2) 'version)))
                  (else (error "Error: cannot parse file name") (newline)))
          (when #f (display "debug:making the object") (newline))
          (make-slackbuild-meta type
                                package
                                version
                                (detect-homepage url package)
                                url
                                md5))))

    (define (lwf_sbo_detect-build-system)
      (cond ((file-exists? "meson.build") 'meson)
            ((file-exists? "CMakeLists.txt") 'cmake)
            ((file-exists? "configure") 'configure)
            ((or (file-exists? "Makefile")
                (file-exists? "GNUmakefile")) 'make)
            (else (error "Build system not identified"))))

    (define (lwf-sbo-detect-bureaucracy)
      (define sentence-end-template ". ")
      (define max-line-lenght-at-sbo 70)

      (define (castrate readme-text)
        (define max-width 72)
        (define max-height 1000)
        "Improve me."
        (run/string (| ; |
                     (cat ,readme-text) (fold -s --width ,max-width) (head -n ,max-height))))

      (define (single-line-description-from filename)
        (run/string
         (| ; |
          (echo ,(cond ((regexp-search (rx (seq (submatch (* any)) ,sentence-end-template ))
                                       (run/string (cat ,filename)))
                        => (lambda (x) (match:substring x 1)))
                       ((with-exception-handler (lambda (_) #f)
                          (lambda () (substring (run/string (cat ,filename)) 0 max-line-lenght-at-sbo)))
                        => (lambda (x) x))
                       (else (error "TODO 1: A better heuristic is needed for short description creation."))))
          (tr --delete "\n" ) (tr --squeeze-repeats " "))))

      (define (short-description-from filename)
        (run/string
         (tr --delete "\n")
         (<< ,(cond ((regexp-search (rx (submatch (= 2 (seq (* any) ,sentence-end-template ))))
                                    (run/string (cat ,filename)))
                     => (lambda (x) (match:substring x 1)))
                    ((with-exception-handler (lambda (_) #f)
                       (lambda () (substring (run/string (cat ,filename)) 0 (* 6 max-line-lenght-at-sbo))))
                     => (lambda (x) x))
                    (else (error "TODO 1: A better heuristic is needed for short description creation."))))))

      (let* ((readme-file (lwf-sbo-original-README-from-readme))
             (castrated-readme-text (castrate readme-file))
             (slack-desc-oneliner (single-line-description-from readme-file))
             (slack-desc-text (short-description-from readme-file)))
        (list (list 'readme castrated-readme-text)
              (list 'oneliner slack-desc-oneliner)
              (list 'slack-desc slack-desc-text))))

    (define (lwf-sbo-original-README-from-readme)
      (cond ((file-exists? "README") "README")
            ((file-exists? "README.md") "README.md")
            (else (error "TODO 3: implement more readme heuristics"))))

    (define lwf-sbo-buildsystem-heuristic-table (make-table))

    (table-set!
     lwf-sbo-buildsystem-heuristic-table
     'meson
     (lambda (package-meta)
       (run (cp ,(path-list->file-name
                  (list lwf_sbo_templates_dir "meson-template.SlackBuild"))
                ,(path-list->file-name
                  (list "." (string-append (slackbuild-meta-slackbuild-name-get package-meta)
                                           ".SlackBuild")))))

       (when #t (display "debug: TODO 4: Systems other than meson are not implemented.") (newline))))


    (define (lwf-sbo-copy-infra package-meta build-system-and-heuristics)
      (define (build-system-from o) (car o))
      (define (readme-from x) (cadr (assoc 'readme (car (cdr x)))))

      (define (lwf-sbo-copy-infra-common)
        (map (lambda (file) (run (cp ,(path-list->file-name (list lwf_sbo_templates_dir file)) ".")))
             (list "README" "slack-desc" "template.info"))
        (rename-file "template.info" (string-append
                                      (slackbuild-meta-slackbuild-name-get package-meta)
                                      ".info")))

      (define (slack-desc-generate)
        (define slack-desc-prologue
"# HOW TO EDIT THIS FILE:
# The \"handy ruler\" below makes it easier to edit a package description.
# Line up the first '|' above the ':' following the base package name, and
# the '|' on the right side marks the last column you can put a character in.
# You must make exactly 11 lines for the formatting to be correct.  It's also
# customary to leave one space after the ':' except on otherwise blank lines.

")
      (define slack-desc-handy-ruler
        "|-----handy-ruler------------------------------------------------------|")

        (let* ((pkgname (slackbuild-meta-slackbuild-name-get package-meta))
               (prefixlen (string-length (slackbuild-meta-slackbuild-name-get package-meta)))
               (prefix1 (make-string prefixlen #\  )))
          (define (oneliner-from x)
            (cadr (assoc 'oneliner (car (cdr x)))))
          (define (short-description-from x)
            (cadr (assoc 'slack-desc (car (cdr x)))))
          (define (make-slack-desc-bottom text)
            (define maxlen (- (string-length slack-desc-handy-ruler) 4))
            (define lines-in-bottom 9)
            (let iter ((index lines-in-bottom)
                       (newtext text)
                       (length (string-length text)))
              (let* ((sublen (min length maxlen))
                     (line-candidate (substring newtext 0 sublen)))
                (if (= 0 index)
                   ""
                   (string-append pkgname ":"
                                  (if (= 0 sublen) "" (string-append " " line-candidate))  "\n"
                                  (iter (- index 1) (substring newtext sublen length)
                                        (- length sublen)))))
            ))
        (string-append slack-desc-prologue prefix1 slack-desc-handy-ruler "\n"
                       pkgname ": " pkgname " ("
                       (substring
                        (oneliner-from build-system-and-heuristics)
                        0 (min (string-length (oneliner-from build-system-and-heuristics))
                               (- (string-length slack-desc-handy-ruler)
                                  (+ 5 (string-length pkgname))))) ")\n"
                       pkgname ":" "\n"
                       (make-slack-desc-bottom
                        (short-description-from build-system-and-heuristics))
                       )
        ))

      ;;>begin body
      (when #f (display "Copying. Build-system=") (display
                                                  (build-system-from
                                                   build-system-and-heuristics)) (newline)
            (display "Copying. README=") (display (readme-from
                                                       build-system-and-heuristics)) (newline))

      (lwf-sbo-copy-infra-common)
      (when #f (display "copied infra") (newline))
      ;;> TODO l.1: cat <<< readme-text > README
      (run (cat) (<< ,(readme-from build-system-and-heuristics)) (> "README"))
      ;;> TODO l.2: generate slack-desc
      (run (cat) (<< ,(slack-desc-generate)) (> "slack-desc"))
      ;;> TODO l.2: generate the info file
      (let ((infofile (string-append (slackbuild-meta-slackbuild-name-get package-meta) ".info"))
            (sbofile (string-append (slackbuild-meta-slackbuild-name-get package-meta) ".SlackBuild")))
        (run (sed "-i"
                  ,(string-append
                    "1s/name of application/"  (slackbuild-meta-slackbuild-name-get package-meta)
                    "/g")
                  ,infofile))
        (when #f (display "name written") (newline))
        (run (sed "-i"
                  ,(string-append
                    "2s/version of application/"  (slackbuild-meta-commit-or-version-string-get package-meta)
                    "/g")
                  ,infofile))
        (when #f (display "version written") (newline))
        (run (sed "-i"
                  ,(string-append
                    "3s|homepage of application|"  (sbo:homepage-get package-meta)
                    "|g")
                  ,infofile))
        (when #f (display "homepage written") (newline))
        (run (sed "-i"
                  ,(string-append
                    "4s|direct download link(s) of application source tarball(s) arch-independent or x86|"
                    (sbo:url-get package-meta)
                    "|g")
                  ,infofile))
        (run (sed "-i"
                  ,(string-append
                    "5s|md5sum(s) of the source tarball(s) defined in DOWNLOAD|"
                    (sbo:md5-get package-meta)
                    "|")
                  ,infofile))
        (run (sed "-i"
                  ,(string-append
                    "6s|direct download link(s) of application source tarball(s), x86_64 only|"
                    ""
                    "|")
                  ,infofile))
        (run (sed "-i"
                  ,(string-append
                    "7s|md5sum(s) of the source tarball(s) defined in DOWNLOAD_x86_64|"
                    ""
                    "|")
                  ,infofile))
        (run (sed "-i"
                  ,(string-append
                    "8s|%README%|"
                    ""
                    "|")
                  ,infofile))
        (run (sed "-i"
                  ,(string-append
                    "9s|name of SlackBuild script maintainer|"
                    lwf-maintainer
                    "|")
                  ,infofile))
        (run (sed "-i"
                  ,(string-append
                    "10s|email address of author|"
                    lwf-maintainer-email
                    "|")
                  ,infofile))
        
      (let ((b (table-ref lwf-sbo-buildsystem-heuristic-table
                          (build-system-from build-system-and-heuristics))))
        (if b (b package-meta) (error "No heuristic defined for " build-system-and-heuristics)))
      ;;> So now we have Readme text, two strings for slack-desc, and the SlackBuild
      ;;> copied into the directory.
      (run (sed "-i"
                ,(string-append
                  "s|PRGNAM=appname|PRGNAM="
                  (slackbuild-meta-slackbuild-name-get package-meta)
                  "|")
                ,sbofile))
      (run (sed "-i"
                ,(string-append
                  "s|VERSION:-1.4.1|VERSION:-"
                  (if (equal? (sbo:commit-or-version-type-get package-meta)
                             'commit)
                     (error "Commit versioning not implemented. TODO: implement commit versioning")
                     (slackbuild-meta-commit-or-version-string-get package-meta))
                  "|")
                ,sbofile))

      (when #t (display "debug: TODO 6: Continue here!
 You need to finish rewriting the slackbuild.
Heuristic dispatch probably should be based on the build system?
 Because in order to rewrite the
some pieces, we need to already know the semi-installed state.
For example, in order to deal with man pages, or info pages,
we need to first try to install a package into DESTDIR, and if
it has not mans or no infos, remove those lines from the
slackbuild. There are other cases like this, and they are all a pain.") (newline)))
      )

;;; The testing package was:
;;; "https://gitlab.gnome.org/GNOME/jsonrpc-glib/-/archive/3.42.0/jsonrpc-glib-3.42.0.tar.gz"


    (define (main args)
      (define arg-url
        (if (>= (length args) 2)
           (cadr args)
           (error "The first argument should be the url! (or maybe, a source/repo notation)")))
      (when #t (display "Processing: ") (display arg-url) (newline))
      (let* ((source-filename (lwf_sbo_fetch_source arg-url)
                              ;;"jsonrpc-glib-3.42.0.tar.gz"
                              ;;"wshowkeys-e8bfc78f08ebdd1316daae59ecc77e62bba68b2b.zip"
                              )
             (source-fullpath (path-list->file-name (list lwf_downloads_dir source-filename)))
             (md5 (car (run/strings (| ;|
                                     (md5sum ,source-fullpath) (awk "{print $1;}")))))
             (package-meta (lwf_sbo_detect_commit_or_version
                            source-filename
                            arg-url
                            md5)))
        ;; (when #f (display "The file we are working with is: ") (display source-filename) (newline))
        ;; (when #t (display package-meta) (newline))
        (run (rm -rf ,(slackbuild-meta-slackbuild-name-get package-meta)))
        (run (mkdir -p ,(slackbuild-meta-slackbuild-name-get package-meta)))
        (chdir (slackbuild-meta-slackbuild-name-get package-meta))
        (create-symlink source-fullpath source-filename)
        (let* ((archive-full-path (resolve-file-name source-filename (cwd)))
               (build-system-and-heuristics
                (with-temporary-unpacked-archive archive-full-path
                                                 (list (lwf_sbo_detect-build-system)
                                                       (lwf-sbo-detect-bureaucracy)))))
          (when #f (display "archive-full-path=") (display archive-full-path) (newline))
          ;; (display "pkg-dir=") (display pkg-dir) (newline)
          (when #f (display "build-system=") (display (car build-system-and-heuristics)) (newline))
          (lwf-sbo-copy-infra package-meta build-system-and-heuristics)
          (when #t
            (display "debug:TODO 5: Maybe we should add some package/script alteration to main?") (newline))
          )))

    ))
