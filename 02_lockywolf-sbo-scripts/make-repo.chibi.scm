#!/usr/bin/env chibi-scheme
;;; Time-stamp: <2023-02-20 13:30:15 lockywolf>
;;;#+Created: <2022-09-03 Mon> : New attempt with Chibi.
;;;#+Author: lockywolf
;;;#+category: admin
;;;#+tags: linux, scheme, slackware, slackbuilds, scsh, heuristics

(import (scheme base)
        (only (scheme process-context) command-line)
        (only (scheme small) write with-output-to-file display exit cadar)
        (only (scheme list) last)
        (only (srfi 166) show pretty displayed written pretty-shared nl) ;; (sheme show)
        (only (srfi 115) regexp-search regexp-match-submatch regexp-split rx regexp-replace-all regexp-extract)
        #;(chibi snow commands)
        #;(chibi snow interface)
        #;(chibi app)
        #;(chibi config)
        #;(chibi pathname)
        #;(chibi process)
        (only (chibi shell) shell shell->string shell->string-list)
        (rename (only (chibi filesystem) with-directory) (with-directory chibi-filesystem-with-directory))
        (only (chibi filesystem) current-directory change-directory create-directory rename-file directory-files file-exists? file-directory? symbolic-link-file file-size delete-file)
        (only (chibi pathname) path-resolve path-normalize path-strip-directory)
        (only (chibi temp-file) call-with-temp-dir)
        (only (chibi crypto md5) md5)
        (only (chibi generic) define-generic define-method generic-add!)
        (only (chibi time) time-year seconds->time current-seconds)
        (only (srfi 145) assume)
)

(define-record-type :slackbuild-meta
  (make-sb-meta name
                commit-or-version-type
                commit-or-version-string
                homepage
                url
                md5
                one-line-description
                short-description
                readme-text
                source-dir-name
                build-system
                source-fullpath
                readme-filename
                source-filename)
  slackbuild-meta?
  (name sb:name)
  (commit-or-version-type sb:version-type)
  (commit-or-version-string sb:version-string)
  (homepage sb:homepage)
  (url sb:url)
  (md5 sb:md5)
  (one-line-description sb:one-line-description)
  (short-description sb:short-description)
  (readme-text sb:readme-text)
  (source-dir-name sb:source-dir-name)
  (build-system sb:build-system)
  (source-fullpath sb:source-fullpath)
  (readme-filename sb:readme-filename)
  (source-filename sb:source-filename)
  )

;;> Auxiliary syntax

(define-syntax showt
  (syntax-rules ()
    ((_ . a)
     (show #t . a))))
(define year-now (number->string (+ 1900 (time-year (seconds->time (current-seconds))))))
;;> Development Settings

(define lwf_debug #f)
(define lwf_debug_parsing_fake_command_line
  (list "./make-repo.chibi.scm"
        ;; "https://gitlab.gnome.org/GNOME/jsonrpc-glib/-/archive/6f6f1e76ecdd7ae63ac043b3094ffbda209041cf/jsonrpc-glib-6f6f1e76ecdd7ae63ac043b3094ffbda209041cf.tar.gz"
        ;; "https://gitlab.gnome.org/GNOME/jsonrpc-glib/-/archive/3.42.0/jsonrpc-glib-3.42.0.tar.gz"
        "https://github.com/vala-lang/vala-language-server/releases/download/0.48.5/vala-language-server-0.48.5.tar.xz"
        ))

;;> Program settings
;;> TODO: replace with (chibi config) ?
(define mr-settings-source-download-dir
  "/home/lockywolf/DevLinux/lwf_downloads_for_slackbuilds")
(define mr-settings-maintainer "anonymous")
(define mr-settings-maintaners-email "anonymous@legion.inc")
(define mr-settings-templates-dir "/home/lockywolf/OfficialRepos/slackbuilds-templates")
(define mr-settings-where-lives "Earth")

;;> auxiliary defines

(define (mr-xor a b) (not (equal? a b)))

(define mr-fs-last-file #f)
(define mr-fs-last-dir #f)
(begin
  (define (lwf_last_proto type)
    (string-copy (shell->string
                  (find "." -maxdepth 1 -type ,type -printf "%C@ - %p\n")
                  (sort -n  )
                  (tail -n 1)
                  (awk "{printf( $3);}")
                  )
                 2 ))
  (set! mr-fs-last-file (lambda () (lwf_last_proto "f")))
  (set! mr-fs-last-dir  (lambda () (lwf_last_proto "d")))
)

;;> defines
(define (mr-fetch-source url)
  (chibi-filesystem-with-directory mr-settings-source-download-dir
                                   (lambda ()
                                     (shell (wget "--unlink" "-nd"  ,url)) ;; This does not check if the file is already there.
                                     (mr-fs-last-file)
                                     )))

(define (is-github-url? url)
  (regexp-search (rx (seq "http" (* "s") "://github.com" (* any))) url))
(define (is-gitlab-gnome-url? url)
  (regexp-search (rx (seq "http" (* "s") "://gitlab.gnome.org" (* any))) url))
(define (is-gitlab-url? url)
  (regexp-search (rx (seq "http" (* "s") "://gitlab.com" (* any))) url))

(define-generic parse-url-into-website-program_name-version)
;;> Returns (program-name version 'commit-or-version homepage constructed-version) ?

(define-method (parse-url-into-website-program_name-version url)
  ;;> Default method, independent of the hosting provider, but hacky.
  (define split-string (regexp-split (rx "/") url))
  (define last-component (last split-string))
  (define last-component-split (regexp-split (rx "-") last-component))
  (define (detect-homepage url name)
    "heuristic to extract the homepage from a url"
    (cond ((regexp-search (rx (seq (submatch (* any)) ,name (* any))) url) => (lambda (x) (regexp-match-submatch x 1)))
          (else (error "Please add more heuristics to detect url!"))))
  (showt "Not found any forge-specific version-detecting method. Using generic approach." nl)
  (cond ((regexp-search (rx (seq (submatch (* any)) "-" (submatch (= 40 (or ("01234567890abcdef")))))) last-component)
         => (lambda (x) (values (regexp-match-submatch x 1) (regexp-match-submatch x 2) 'commit (detect-homepage url (regexp-match-submatch x 1)))))
        ((regexp-search (rx (seq (submatch (* any)) (or "-" "_") (submatch (seq (or ("01234567890")) (* (or ("01234567890._"))) (or ("01234567890")) )))) last-component) ;; todo: improve heuristic
         => (lambda (x) (values (regexp-match-submatch x 1) (regexp-match-submatch x 2) 'version (detect-homepage url (regexp-match-submatch x 1)))))
        (else (error "Error: cannot parse file name") (newline))))

(define-method (parse-url-into-website-program_name-version (url is-github-url?))
  (define (make-homepage-url group project)
    (string-append "https://github.com/" group "/" project))
  (define (make-empitical-version group project commit)
    (call-with-temp-dir "template"
     (lambda (path preserve)
       (chibi-filesystem-with-directory
        path
        (lambda ()
          (shell (git clone ,(make-homepage-url group project) target))
          (change-directory "target")
          (shell (git checkout ,commit))
          (let* ((date-ver (car (shell->string-list
                                 (git --no-pager log --date=raw -n 1)
                                 (grep Date)
                                 (awk "{print $2;}" ))))
                 (tag-string (shell->string
                              (git "--no-pager" log "--decorate=full" )
                              (grep "tag:")
                              (head "-n" 1)))
                 (version-match (regexp-search
                                 (rx (* any) "refs/tags/" (* any)
                                     (submatch-named version (seq (+ digit) (* any)))
                                     (or "," ")") (* any))
                                 tag-string))
                 (_ (if (not version-match) #f #; (error "version not matched in a tag!")))
                 (last-tag-ver  (guard (ex (regexp-match-submatch version-match
                                                                  'version))
                                  "0"))
                 (empirical-version (string-append last-tag-ver ".0." date-ver "." (substring commit 0 8))))
            empirical-version))))))

  "https://github.com/dino/dino/archive/b8e84c83268a11ae41ad1d673999362427fd755c.tar.gz"
  "https://github.com/dino/dino/archive/refs/tags/v0.3.0.tar.gz"
  "https://github.com/dino/dino/releases/download/v0.3.0/dino-0.3.0.tar.gz"

  (let-values (((parsed-url version-or-commit) (cond ((regexp-search (rx ;; commit branch
                                           "http" (? "s") "://github.com"
                                           "/"
                                           (submatch-named group (+ (or alphanumeric "-")))
                                           "/"
                                           (submatch-named project (+ (or alphanumeric "-")))
                                           "/" "archive" "/"
                                           (submatch-named commit (= 40 (or ("01234567890abcdef"))))
                                           (submatch-named rest (* any))) url)
                           => (lambda (x) (values x 'commit)))
                          ((regexp-search (rx ;; version branch
                                           "http" (? "s") "://github.com"
                                           "/"
                                           (submatch-named group (+ (or alphanumeric "-")))
                                           "/"
                                           (submatch-named project (+ (or alphanumeric "-")))
                                           "/" "releases" "/" "download" "/" "v"
                                           (submatch-named version (seq (or ("01234567890")) (* (or ("01234567890."))) (or ("01234567890")) ))
                                           (submatch-named rest (* any))) url)
                           => (lambda (x) (values x 'version)))
                          ((regexp-search (rx ;; version branch
                                           "http" (? "s") "://github.com"
                                           "/"
                                           (submatch-named group (+ (or alphanumeric "-")))
                                           "/"
                                           (submatch-named project (+ (or alphanumeric "-")))
                                           "/" "archive" "/" "refs" "/" "tags" "/" (? "v") (? (* alpha) "_")
                                           (submatch-named version (seq (or ("01234567890")) (* (or ("01234567890._"))) (or ("01234567890")) ))
                                           (submatch-named rest (* any))) url)
                           => (lambda (x) (values x 'version)))
                          (else (error "Unsupported github URL.")))))
    (lambda (x) (showt "string-rest is " (regexp-match-submatch x 'rest) nl))
    (values
     (regexp-match-submatch parsed-url 'project)
     (guard (ex (else (make-empitical-version
                       (regexp-match-submatch parsed-url 'group)
                       (regexp-match-submatch parsed-url 'project)
                       (regexp-match-submatch parsed-url 'commit))))
       (regexp-match-submatch parsed-url 'version))
     version-or-commit
     (make-homepage-url (regexp-match-submatch parsed-url 'group)
                        (regexp-match-submatch parsed-url 'project)))))

(define (mr-detect-hosting-provider-by-url url)
  ;;> TODO: this place should be extensible.
  (cond ((is-github-url? url)         'github-com)
        ((is-gitlab-gnome-url? url)   'gitlab-gnome-org)
        ((is-gitlab-url? url)         'gitlab-com)
        (else 'cannot-detect-hosting-provider)))

(define (mr-url-has-commit-like-sequence-in-it? url)
;;> 6f6f1e76ecdd7ae63ac043b3094ffbda209041cf
  (cond ((regexp-search (rx (seq (* any) (submatch (= 40 (or lower-case numeric))) (* any))) url)
         => (lambda (match) (regexp-match-submatch match 1)))
        (else #f)))

(define-record-type :build-system
  (make-build-system
   name
   script-name
   interimise-script)
  build-system?
  (name bs:name)
  (script-name bs:script-name)
  (interimise-script bs:interimise-script)
  )
;#(define-record-type :build-system  (make-build-system   name)  build-system?  (name bs:name))

(define-generic detect-build-system)

(define-method (detect-build-system)
  (error "No method detector matched."))

;; Define-detect-build-system and define-method do not compose
;; I think, this place is begging for a CK-macro, which should, in theory,
;; give us composable macros, but I have not yet understood how it
;; can be used here. Nevertheless, I am leaving a comment here, in order
;; to return to it later. TODO

#;(define-syntax define-detect-build-system
  "This code does not work, because (call-next-method) is not passed."
  (syntax-rules ()
    ((_ file build-system-name)
     (define-method (detect-build-system)
       (if (file-exists? file)
                        build-system-name
                        (call-next-method))))))

(define-syntax define-detect-build-system
  (syntax-rules ()
    ((_ file build-system-name script-name interimiser)
     (generic-add! detect-build-system (list)
                   (lambda (next)
                     (if (file-exists? file)
                        (make-build-system build-system-name script-name interimiser)
                        (next)))))))


(define-detect-build-system "meson.build"
                          'meson
                          "meson-template.SlackBuild"
                          (lambda (interim-script-name)
                            (shell (sed "-i" "/cd \\.\\./,$d" ,interim-script-name))
                            #;(error "TODO")))
(define-detect-build-system "CMakeLists.txt"
                          'cmake
                          "cmake-template.SlackBuild"
                          (lambda (interim-script-name)
                            (shell (sed "-i" "-n" "1,/make install/p" ,interim-script-name))))

(define-detect-build-system "configure.ac"
                          'configure.ac
                          "autotools-template.SlackBuild"
                          (lambda (interim-script-name)
                            (shell (sed "-i" "-n" "1,/make install/p" ,interim-script-name))
                            (shell (sed "-i" "/CXXFLAGS=/aautoreconf -i"  ,interim-script-name))))

(define-detect-build-system "autogen.sh"
                          'autogen.sh
                          "autotools-template.SlackBuild"
                          (lambda (interim-script-name)
                            (shell (sed "-i" "-n" "1,/make install/p" ,interim-script-name))
                            (shell (sed "-i" "s|\.\/configure|./autogen.sh|g" ,interim-script-name))))

(define-detect-build-system "configure"
                          'configure
                          "autotools-template.SlackBuild"
                          (lambda (interim-script-name)
                            ;;--disable-static
                            (shell (sed "-i" "/--disable-static/a--enable-appliance=no \\\\"  ,interim-script-name))
                            (shell (sed "-i" "-n" "1,/make install/p" ,interim-script-name))))


#;(showt "Debug: " (macroexpand '(define-method (detect-build-system)
                              (if (file-exists? file)
                                 build-system-name
                                 (call-next-method)))) nl)


(define (mr-detect-source-name-dir-and-build-system path-to-archiveball)
  ;;> TODO: this place needs genericity over archive formats
  ;;> cond regexp-match tar.gz, etc.
  (define sentence-end-template ". ")
  (define max-line-lenght-at-sbo 70)
  #;(define (detect-build-system)
    ;;> This place can be made more generic with tables. '(detector symbol)
      (cond ((file-exists? "meson.build") 'meson)
            ((file-exists? "configure") 'configure)
            ((file-exists? "CMakeLists.txt") 'cmake)
            ((file-exists? "Makefile.am") 'automake)
            ((file-exists? "configure.ac") 'autoconf)
            ((or (file-exists? "Makefile")
                (file-exists? "GNUmakefile")) 'make)
            (else (error "Build system not identified"))))
  (define (detect-readme-file)
    (showt "Custom-readme-file: " custom-readme-file)
    (cond (custom-readme-file custom-readme-file)
          ((file-exists? "README.rst") "README.rst")
          ((file-exists? "README")    "README")
          ((file-exists? "README.md") "README.md")
          (else (error "TODO 3: implement more readme heuristics"))))
  (define (README-text-from-file readme-file)
    ;;> Take a file and bend it into readme format.
    (define (better-format readme-text)
        (define max-width 72)
        (define max-height 1000)
        "Improve me."
        (shell->string (cat ,readme-text) (fold -s --width ,max-width) (head -n ,max-height)))
    (better-format readme-file))

  (define (short-description-from filename)
    (define file-contents (shell->string (cat ,filename)))
    (define dirty-description (cond ((regexp-search (rx (submatch (= 2 (seq (* any) ,sentence-end-template )))) file-contents )         => (lambda (x)  (regexp-match-submatch x 1)))
                              ((with-exception-handler (lambda (_)  #f) (lambda () (substring file-contents 0 (* 6 max-line-lenght-at-sbo))))
                               => (lambda (x)  x))
                              (else (error "TODO 1: A better heuristic is needed for short description creation."))))
    (let ((retval (regexp-replace-all (rx (or #\return #\newline)) dirty-description "")))
      retval))

  (define (single-line-description-from filename)
    (showt "Readme-filename=" filename nl)
    (shell->string
     (echo ,(cond ((regexp-search (rx (seq (submatch (* any)) ,sentence-end-template )) (shell->string (cat ,filename)))           => (lambda (x) (regexp-match-submatch x 1)))
                  ((guard (c
                           (else #f))
                     (substring (shell->string (cat ,filename)) 0 max-line-lenght-at-sbo)))
                  (else (error "TODO 1: A better heuristic is needed for single-line description creation."))))
     (tr --delete "\n" ) (tr --squeeze-repeats " ")))

  (call-with-temp-dir "template"
    (lambda (path preserve)
      (chibi-filesystem-with-directory path
        (lambda ()
          ;;> This place can be made more generic with archive format detectors.
          (shell (tar xvf ,path-to-archiveball) (> "/dev/null"))
          (showt (directory-files ".") nl)
          (let ((source-dir-name (if (= (length (directory-files ".")) 3)
                                (car (directory-files "."))
                                (showt "The archive has more than just the root directory. Such cases are not implemented yet." nl)))) ;;> TODO
            (change-directory source-dir-name)
            (let ((readme-file (detect-readme-file)))
              (list ;;> TODO: this place should return an object with build particulars.
               (list 'dbg-source source-dir-name)
               (list 'dbg-buildsystem (detect-build-system))
               (list 'dbg-readme-text (README-text-from-file readme-file))
               (list 'dbg-short-description-for-slackdesc (short-description-from readme-file))
               (list 'dbg-oneliner (single-line-description-from readme-file))
               (list 'dbg-readme-filename readme-file)
               ))
            )
          )))))

(define (mr-create-infra-common package-meta)
  (define (adjust-infofile)
    (let
        ((infofile (string-append (sb:name package-meta) ".info"))
         (sed-list (list
                    (string-append "1s/name of application/" (sb:name package-meta) "/g")
                    (string-append
                     "2s/version of application/"  (sb:version-string package-meta) "/g")
                    (string-append
                     "3s|homepage of application|"  (sb:homepage package-meta) "|g")
                    (string-append
"4s|direct download link(s) of application source tarball(s) arch-independent or x86|"
(sb:url package-meta) "|g")
                    (string-append
"5s|md5sum(s) of the source tarball(s) defined in DOWNLOAD|"
(sb:md5 package-meta) "|")
                    (string-append
"6s|direct download link(s) of application source tarball(s), x86_64 only|" "" "|")
                    (string-append
                     "7s|md5sum(s) of the source tarball(s) defined in DOWNLOAD_x86_64|" "" "|")
                    (string-append "8s|%README%|" "Generator does not support extracting dependencies:( do it yourself" "|")
                    (string-append "9s|name of SlackBuild script maintainer|"
                                   mr-settings-maintainer "|")
                    (string-append
                    "10s|email address of author|" mr-settings-maintaners-email "|"))))
      (map (lambda (x) (shell (sed "-i" ,x ,infofile))) sed-list)))

  (define (slack-desc-generate)
    (define slack-desc-prologue
      "# HOW TO EDIT THIS FILE:
# The \"handy ruler\" below makes it easier to edit a package description.
# Line up the first '|' above the ':' following the base package name, and
# the '|' on the right side marks the last column you can put a character in.
# You must make exactly 11 lines for the formatting to be correct.  It's also
# customary to leave one space after the ':' except on otherwise blank lines.

")
    (define slack-desc-handy-ruler
      "|-----handy-ruler------------------------------------------------------|")

    (let* ((pkgname (sb:name package-meta))
           (prefixlen (string-length pkgname))
           (prefix1 (make-string prefixlen #\  )))
      (define (make-slack-desc-bottom text)
        (define maxlen (- (string-length slack-desc-handy-ruler) 4))
        (define lines-in-bottom 9)
        (let iter ((index lines-in-bottom)
                   (newtext text)
                   (length (string-length text)))
          (let* ((sublen (min length maxlen))
                 (line-candidate (substring newtext 0 sublen)))
            (if (= 0 index)
               ""
               (string-append pkgname ":"
                              (if (= 0 sublen) "" (string-append " " line-candidate))  "\n"
                              (iter (- index 1) (substring newtext sublen length)
                                    (- length sublen)))))
          ))
      (string-append slack-desc-prologue prefix1 slack-desc-handy-ruler "\n"
                     pkgname ": " pkgname " ("
                     (substring
                      (sb:one-line-description package-meta)
                      0 (min (string-length (sb:one-line-description package-meta))
                             (- (string-length slack-desc-handy-ruler)
                                (+ 5 (string-length pkgname))))) ")\n"
                                pkgname ":" "\n"
                                (make-slack-desc-bottom
                                 (sb:short-description package-meta)))))
  ;;> assumes current directory is where we build the slackbuild infra
  (map (lambda (file) (shell (cp ,(path-normalize (path-resolve file mr-settings-templates-dir)) ".")))
       (list ;; "README"
             "slack-desc"
             "template.info"
             "doinst.sh"))
  (rename-file "template.info" (string-append (sb:name package-meta) ".info"))
  (with-output-to-file "slack-desc" (lambda () (display (slack-desc-generate))))
  (with-output-to-file "README" (lambda () (display (sb:readme-text package-meta))))
  (adjust-infofile)
  (symbolic-link-file (sb:source-fullpath package-meta) (sb:source-filename package-meta))
  )

;;> Generic methods.
(define-generic mr-create-infra-particular)
(define-method (mr-create-infra-particular package-meta)
  (error "Unsupported build system." (sb:build-system package-meta)))

(define-method (mr-create-infra-particular package-meta)
  (call-with-temp-dir "for-building-slackbuild"
  (lambda (interim-prefix preserve-temporary-dir)
    (define target-buildscript-name (string-append (sb:name package-meta) ".SlackBuild"))
    (define interim-script-name (string-append "interim-" target-buildscript-name))
    (define interim-installdir (string-append interim-prefix "/package-" (sb:name package-meta)))
    (define (directory-has-static-libs? interim-installdir)
      (chibi-filesystem-with-directory
       interim-installdir
       (lambda ()
         (let ((la-list (append (shell->string-list (find "./lib64" "-iname" "*.la"))
                                (shell->string-list (find "./usr/lib64" "-iname" "*.la")))))
           (if (< 0 (length la-list))
              (begin (showt "found static libs" nl) #t)
              (begin (showt "no static libs" nl) #f))))))
    (define (directory-has-info-dir? interim-installdir)
      (chibi-filesystem-with-directory
       interim-installdir
       (lambda ()
         (if (file-exists? "./usr/info/dir")
            (begin (showt "found infodir file" nl) #t)
            (begin (showt "no infodir file" nl) #f)))))
    (define (directory-has-info-files? interim-installdir)
      (chibi-filesystem-with-directory
       interim-installdir
       (lambda ()
         (if (< 0 (length (shell->string-list (find "./usr/info" "-iname" "*.info"))))
            (begin (showt "found info files" nl) #t)
            (begin (showt "no info files" nl) #f)))))
    (define (directory-has-pearls? interim-installdir)
      (chibi-filesystem-with-directory
       interim-installdir
       (lambda ()
         (if (< 0 (length (shell->string-list (find "./" "-iname" "perllocal.pod"))))
            (begin (showt "found perl files" nl) #t)
            (begin (showt "no perl files" nl) #f)))))
    (define (directory-has-mans? interim-installdir)
      (chibi-filesystem-with-directory
       interim-installdir
       (lambda ()
         (if (file-exists? "./usr/man")
            (begin (showt "found mandir" nl) #t)
            (begin (showt "no mandir" nl) #f)))))
    (define (directory-has-initscripts? interim-installdir)
      (chibi-filesystem-with-directory
       interim-installdir
       (lambda ()
         (if (< 0 (length (shell->string-list (find "./etc/rc.d" ))))
            (begin (showt "found initfiles" nl) #t)
            (begin (showt "no initfiles" nl) #f)))))
    (define (directory-has-schemas? interim-installdir)
      (chibi-filesystem-with-directory
       interim-installdir
       (lambda ()
         (if (< 0 (length (shell->string-list (find "./etc/gconf/schemas/" ))))
            (begin (showt "found schemas" nl) #t)
            (begin (showt "no schemas" nl) #f)))))
    (define (directory-has-configs? interim-installdir)
      (chibi-filesystem-with-directory
       interim-installdir
       (lambda ()
         (if (< 0 (length (shell->string-list (find "./etc/" -not -path "./etc/rc.*" ))))
            (begin (showt "found configs" nl) #t)
            (begin (showt "no configs" nl) #f)))))
    (define (directory-has-giomodules? interim-installdir)
      (chibi-filesystem-with-directory
       interim-installdir
       (lambda ()
         (if (< 0 (length (shell->string-list (find "./usr/lib64/gio/modules/" ))))
            (begin (showt "found gio modules" nl) #t)
            (begin (showt "no gio modules" nl) #f)))))

    (define (directory-has-usr-share-glib2-shemas? interim-installdir)
      (chibi-filesystem-with-directory
       interim-installdir
       (lambda ()
         (if (< 0 (length (shell->string-list (find "./usr/share/glib-2.0/schemas/" ))))
            (begin (showt "found glib2 schemas" nl) #t)
            (begin (showt "no glib2 schemas" nl) #f)))))

    (define (directory-has-meme-database? interim-installdir)
      (chibi-filesystem-with-directory
       interim-installdir
       (lambda ()
         (if (< 0 (length (shell->string-list (find "./usr/share/mime/" ))))
            (begin (showt "found meme files" nl) #t)
            (begin (showt "no meme files" nl) #f)))))

    (define (directory-has-dotdesktop-files? interim-installdir)
      (chibi-filesystem-with-directory
       interim-installdir
       (lambda ()
         (if (< 0 (length (shell->string-list (find "./usr/share/applications/" ))))
            (begin (showt "found desktop files" nl) #t)
            (begin (showt "no desktop files" nl) #f)))))

    (define (directory-get-iconsets interim-installdir)
      (if (change-directory "usr/share/icons")
         (directory-files)
         (list)))

    (define (directory-get-infofiles interim-installdir)
      (if (change-directory "usr/info")
         (directory-files)
         (list)))

    (shell (cp ,(path-normalize
                 (path-resolve
                  (bs:script-name (sb:build-system package-meta))  ;;"meson-template.SlackBuild"
                  mr-settings-templates-dir))
               ,(path-normalize
                 (path-resolve target-buildscript-name "./"))))
    (let ((cut-slackbuild (shell->string (sed "24,$s/#.*$//g" ,target-buildscript-name) (grep "-v" "-E" "^$") )))
      (with-output-to-file target-buildscript-name
        (lambda () (display cut-slackbuild))))
    (let ((sed-list (list (string-append "s/<appname>/" (sb:name package-meta) "/g")
                          (string-append "s/appname/" (sb:name package-meta) "/g")
                          (string-append "s/<year>/" year-now "/g")
                          (string-append "s/<you>/" mr-settings-maintainer "/g")
                          (string-append "s/<where you live>/" mr-settings-where-lives "/")
                          (string-append "s|tar xvf $CWD/$PRGNAM-$VERSION.tar.gz|"
                                         (string-append "tar xvf $CWD/" (sb:source-filename package-meta)) "|")
                          (string-append
                           "s|VERSION:-1.4.1|VERSION:-"
                           (sb:version-string package-meta)
                           #;(if (equal? (sb:version-type package-meta) 'commit)
                           (error "Commit versioning not implemented. TODO: implement")
                           (sb:version-string package-meta)) "|"))))
      (map (lambda (x) (shell (sed "-i" ,x ,target-buildscript-name))) sed-list))

    (showt "How do we update the SlackBuild here (for perl, info, and such)?" nl)
    (shell (sed "-i" ,(string-append "s/<documentation>/" (sb:readme-filename package-meta) "/")
                ,target-buildscript-name ))
    (when (file-directory? "doc")
      (showt "copying the doc directory in full" nl)
      (shell (sed "-i" ,(string-append "s/cp -a \/cp -aR doc \/") ,target-buildscript-name)))

    ;;> sed out (rm -rf $PRGNAM-$VERSION)
    (shell (sed "-i" ,(string-append "\\|rm -rf $PRGNAM-$VERSION|s|rm -rf $PRGNAM-$VERSION|rm -rf " (sb:source-dir-name package-meta) "|") ,target-buildscript-name))

    ;;> sed out tar xvf source.tar.gz
    (shell (sed "-i" ,(string-append "\\|tar xvf $CWD/$PRGNAM-$VERSION.tar.gz|s|tar xvf $CWD/$PRGNAM-$VERSION.tar.gz|tar xvf $CWD/" (sb:source-filename package-meta) "|") ,target-buildscript-name))

    ;;> sed out (cd $PRGNAM-$VERSION)
    (shell (sed "-i" ,(string-append "\\|cd $PRGNAM-$VERSION|s|cd $PRGNAM-$VERSION|cd " (sb:source-dir-name package-meta) "|") ,target-buildscript-name))
    ;; Now let us run it and see if it works.
    (shell (rm "-rf" ,(string-append "interim-" target-buildscript-name)))
    (shell (cp ,target-buildscript-name ,(string-append "interim-" target-buildscript-name)))
    #;(shell (sed "-i" "/cd \\.\\./,$d" ,interim-script-name))
    ((bs:interimise-script (sb:build-system package-meta)) interim-script-name)

    (shell (sed "-i" ,(string-append "s|/tmp/SBo|" interim-prefix "|g") ,interim-script-name))
    (shell (sed "-i" "s|chown -R root:root .||g" ,interim-script-name))
    (let ((exitstatus (cadar (shell (bash ,interim-script-name)))))
      (when (not (= 0 exitstatus))
        (showt "Interim build has failed, check: " interim-installdir nl)
        (preserve-temporary-dir)
        (error "Interim script exit status not 0" exitstatus)))
    (showt "Finished interim script. Determining cleanup." nl)

    (when (not (directory-has-static-libs? interim-installdir))
      (shell (sed "-i" "s|rm -f \\$PKG/{,usr/}lib\\${LIBDIRSUFFIX}/\\*.la||"
                  ,target-buildscript-name)))

    (when (not (directory-has-info-dir? interim-installdir))
      (showt "deleting infodir deleter" nl)
      (shell (sed "-i" "s|rm -f $PKG/usr/info/dir||" ,target-buildscript-name)))

    (when (not (directory-has-info-files? interim-installdir))
      (showt "deleting infofiles gzipper" nl)
      (shell (sed "-i" "s|gzip -9 $PKG/usr/info/\\*.info\\*||g" ,target-buildscript-name)))

    (when (not (directory-has-pearls? interim-installdir))
      (showt "deleting perl deleter" nl)
      ;;(shell (sed "-i" "/find $PKG -name perllocal.pod/,+3d" ,target-buildscript-name))
      (let ((t (shell->string (sed "/find \$PKG -name perllocal/,/xargs rm \-f/d" ,target-buildscript-name))))
      (with-output-to-file target-buildscript-name
        (lambda () (display t)))))

    ;;> TODO: Add stuff to unconditionally move /usr/share/man to /usr/man

    (when (not (directory-has-mans? interim-installdir))
      (showt "deleting man compressor" nl)
      (shell (sed "-i" "\\|PKG/usr/man|d" ,target-buildscript-name)))

    (if (not (directory-has-initscripts? interim-installdir))
        (begin
          (showt "deleting initscripts permission-preserver" nl)
          (shell (sed "-i" "\\|preserve_perms etc|d" "doinst.sh"))
          (shell (sed "-i" "\\|preserve_perms\\(\\)|,+9d" "doinst.sh"))
          )
        (begin
          (showt "You need to adjust initscripts permission perserver in doinst.sh manually!" nl)
          (shell (sed "-i" "\\|preserve_perms etc|s|preserve_perms etc/rc.d/rc.INIT.new|; echo ERROR install initfile!; exit 1; preserve_perms etc/rc.d/rc.INIT.new|" "doinst.sh")))
        )

    (if (not (directory-has-schemas? interim-installdir))
        (begin
          (showt "deleting shemas installer" nl)
          (shell (sed "-i" "\\|schema_install blah|d" "doinst.sh"))
          (shell (sed "-i" "\\|schema_install\\(\\)|,+6d" "doinst.sh")))
        (begin
          (showt "You need to adjust schemas in doinst.sh manually!" nl)
          (shell (sed "-i" "\\|schema_install blah|s|schema_install blah.schemas|; echo ERROR adjust schemas; exit 1; schema_install blah.schemas|" "doinst.sh"))))

    (if (not (directory-has-configs? interim-installdir))
        (begin
          (showt "deleting configs newifier" nl)
          (shell (sed "-i" "\\|config etc/configfile.new|d" "doinst.sh"))
          (shell (sed "-i" "\\|config\\(\\)|,+11d" "doinst.sh")))
        (begin
          (showt "You need to adjust schemas in doinst.sh manually!" nl)
          (shell (sed "-i" "\\|config etc/configfile.new|s|config etc/configfile.new|; echo ERROR newify configs; exit 1; config etc/configfile.new|" "doinst.sh"))))

    (if (not (directory-has-dotdesktop-files? interim-installdir))
        (begin
          (showt "deleting dotdesktop database updater" nl)
          (shell (sed "-i" "\\|-x /usr/bin/update-desktop-database|,+2d" "doinst.sh")))
        (begin
          (showt "not deleting dotdesktop database updater" nl)))

    (if (not (directory-has-meme-database? interim-installdir))
        (begin
          (showt "deleting meme database updater" nl)
          (shell (sed "-i" "\\|-x /usr/bin/update-mime-database|,+2d" "doinst.sh")))
        (begin
          (showt "not deleting meme database updater" nl)))

    (let ((iconsets (directory-get-iconsets interim-installdir))
          (iconset-template
           "

if [ -e usr/share/icons/hicolor/icon-theme.cache ]; then
  if [ -x /usr/bin/gtk-update-icon-cache ]; then
    /usr/bin/gtk-update-icon-cache -f usr/share/icons/hicolor >/dev/null 2>&1
  fi
fi

"))
      (shell (sed "-i" "\\|usr/share/icons/hicolor/icon-theme.cache|,+4d" "doinst.sh"))
      (if (not (null? iconsets))
          (map (lambda (theme-name)
                 (showt "updating cache for icon theme " theme-name nl)
                 (shell (sed "-i" ,(string-append "\\|# If other icon themes are installed, then add to/modify this as needed|a" (regexp-replace-all (rx "hicolor") iconset-template theme-name)) "doinst.sh"))) iconsets))
      (shell (sed "-i" "\\|# If other icon themes are installed, then add to/modify this as needed|d" "doinst.sh")))

    (if (not (directory-has-usr-share-glib2-shemas? interim-installdir))
        (begin
          (showt "deleting schemas compiler" nl)
          (shell (sed "-i" "\\|-e usr/share/glib-2.0/schemas|,+4d" "doinst.sh")))
        (begin
          (showt "not deleting schemas compiler" nl)))

    (if (not (directory-has-giomodules? interim-installdir))
       (begin
         (showt "deleting giomodules querier" nl)
         (shell (sed "-i" "\\|@LIBDIR@|d" "doinst.sh")))
       (begin
         (showt "not deleting giomodules querier" nl)
         (shell (sed "-i" "\\|@LIBDIR@|s|@LIBDIR@|/usr/lib64/g" "doinst.sh"))))

    (let ((infofiles (directory-get-infofiles interim-installdir))
          (chrootline-template
           "  chroot . /usr/bin/install-info --info-dir=/usr/info /usr/info/blah.gz 2> /dev/null
"))
      (if (not (null? infofiles))
         (begin
           (shell (sed "-i" "\\|chroot \\. /usr/bin/install\\-info|d"))
           (map (lambda (infofile)
                  (showt "installing infofile " infofile nl)
                  (shell (sed "-i" (string-append
                                    "\\|-x /usr/bin/install\\-info|+1i"
                                    (regexp-replace-all
                                     (rx "/usr/info/blah\\.gz") chrootline-template
                                     (string-append "/" infofile)))
                              "doinst.sh"))
                  ) infofiles))
         (shell (sed "-i" "\\|-x /usr/bin/install-info|,+2d" "doinst.sh"))
         ))
    (shell (sed "-i" "\\|^$|d" "doinst.sh"))
    (when (= 0 (file-size "doinst.sh"))
      (delete-file "doinst.sh")
      (shell (sed "-i" "\\|doinst.sh|d" ,target-buildscript-name )))
    )))


;;> Program itself

(showt "Welcome to the SlackBuild hack." nl)
(showt "Parsing: ")

(define arguments (if lwf_debug
                lwf_debug_parsing_fake_command_line
                (command-line) ))

(showt "arguments=" arguments nl)
(define arg-url (guard (condition
                   (else
                    (begin
                      (showt "Usage: $program url")
                      (error "Arguments invalid:" arguments))))
               (cadr arguments)))

(define custom-readme-file (guard (condition
                              (else
                               (begin
                                 #;(showt "Condition: " (written condition) nl)
                                 #;(error "Debug:" arguments)
                                 #f)))
                        (list-ref arguments 2)))

(showt "Working with: " arg-url nl)

(showt "Custom readme: " custom-readme-file nl)

;;(define hosting-provider (mr-detect-hosting-provider-by-url arg-url))

;;(showt "Hosting provider detected: " hosting-provider nl)

;;(define using-commit-or-not (mr-url-has-commit-like-sequence-in-it? arg-url))

;;(showt "Are we using commit (heuristic): " using-commit-or-not nl)

(define-values (program-name program-version version-type program-homepage)
  (parse-url-into-website-program_name-version arg-url))

(showt "Program-name: " program-name nl)
(showt "Version: " program-version nl)
(showt "Working with a commit?: " version-type nl)
(showt "Homepage: " program-homepage nl)

(define source-filename (mr-fetch-source arg-url))

(showt "Downloaded: " source-filename nl)

(define source-fullpath (path-normalize (path-resolve  source-filename mr-settings-source-download-dir )))

(showt "Full path to the downloaded file: " source-fullpath nl)

(define source-md5 (md5 source-fullpath))

(showt "Source md5 hash: " source-md5 nl)

(define build-system-and-simple-extractions (mr-detect-source-name-dir-and-build-system source-fullpath))

(showt "Detected stuff from unpacked source: " "Too long to show" #;(pretty build-system-and-simple-extractions) nl)

(showt "Making the metadata object." nl)
(define  slackbuild-metadata-object
  (assume (make-sb-meta program-name
                        version-type
                        program-version
                        program-homepage
                        arg-url
                        source-md5
                        (cadr (list-ref build-system-and-simple-extractions 4))
                        (cadr (list-ref build-system-and-simple-extractions 3))
                        (cadr (list-ref build-system-and-simple-extractions 2))
                        (cadr (list-ref build-system-and-simple-extractions 0))
                        (cadr (list-ref build-system-and-simple-extractions 1))
                        source-fullpath
                        (cadr (list-ref build-system-and-simple-extractions 5))
                        source-filename)))


(showt "Full metadata object: " "<too long to show>" #;(pretty-shared slackbuild-metadata-object) nl)

(showt "Detected build system: " (sb:build-system slackbuild-metadata-object) nl)

(showt "Start making the SlackBuild directory." nl)

(create-directory (sb:name slackbuild-metadata-object))
(change-directory (sb:name slackbuild-metadata-object))

(mr-create-infra-common slackbuild-metadata-object)

(mr-create-infra-particular slackbuild-metadata-object)

;;> TMP=${TMP:-/tmp/SBo}
;;> PKG=$TMP/package-$PRGNAM
;;> TODO: dependency detection
;;> TODO: commit detector for gitlab
;;> TODO: improve schema installer, 
;;> TODO: improve config newifier
;;> TODO: improve initscript newifier
