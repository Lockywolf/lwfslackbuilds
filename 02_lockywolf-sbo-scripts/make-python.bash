#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2023-12-04 11:25:35 lockywolf>
#+title: 
#+author: 
#+date: 
#+created: <2023-12-04 Mon 10:06:25>
#+refiled:
#+language: bash
#+category: 
#+tags: 
#+creator: Emacs-30.0.50/org-mode-9.6.8

set -x
# https://files.pythonhosted.org/packages/39/c8/9039c7503577de08a3f4c81e7619583efdc16030da6d1a25268d3dca49c8/pyppmd-1.1.0.tar.gz
l_url_regex='https://files.pythonhosted.org/packages/[[:alnum:]]{2}/[[:alnum:]]{2}/[[:alnum:]]+/([[:alnum:]]+)-([[:digit:].]+).tar.gz'
url="$1"
if [[ "$url" =~ $l_url_regex ]] ; then
  l_package="${BASH_REMATCH[1]}"
  l_version="${BASH_REMATCH[2]}"
  printf "url matches. package=%s version=%s\n" "$l_pacakage" "$l_version" 1>&2
else
  printf "url does not match\n" 1>&2
  exit 1
fi

#set -e

printf "making dir for %s\n" "$1"
mkdir python3-$l_package
cd python3-$l_package

cp /home/lockywolf/OfficialRepos/slackbuilds-templates/template.info python3-$l_package.info

sed -i "s/name of application/python3-$l_package/g" python3-$l_package.info
sed -i "s/version of application/$l_version/g" python3-$l_package.info
sed -i "s|homepage of application|https://pypi.org/project/$l_package|g" python3-$l_package.info
sed -i "s@DOWNLOAD=.*@DOWNLOAD=\"$url\"@g" python3-$l_package.info

read l_md5sum < <(
  l_tmp=$(mktemp -d)
  cd "$l_tmp"
  wget "$url" 1>/dev/null 2>&1
  md5sum $l_package-$l_version.tar.gz | awk '{print $1;}'
  rm -rf $l_package-$l_version.tar.gz
  cd ..
  rm -rf $l_tmp
)

printf "md5sum=%s\n" "$l_md5sum"
sed -i "s/MD5SUM=.*/MD5SUM=\"$l_md5sum\"/g" python3-$l_package.info
sed -i "s/DOWNLOAD_x86_64=.*/DOWNLOAD_x86_64=\"\"/g" python3-$l_package.info
sed -i "s/MD5SUM_x86_64=.*/MD5SUM_x86_64=\"\"/g" python3-$l_package.info
sed -i "s/REQUIRES=.*/REQUIRES=\"broken\"/g" python3-$l_package.info
sed -i "s/MAINTAINER=.*/MAINTAINER=\"Lockywolf\"/g" python3-$l_package.info
sed -i "s/EMAIL=.*/EMAIL=\"for_sbo.python3-${l_package}_$(date --iso=date)@lockywolf.net\"/g" python3-$l_package.info

printf "Editing .info done\n"

cp /home/lockywolf/OfficialRepos/slackbuilds-templates/python-template.SlackBuild python3-$l_package.SlackBuild

sed -i "s/<appname>/python3-$l_package/g" python3-$l_package.SlackBuild
sed -i "s/<year>/$(date +'%Y'), /g" python3-$l_package.SlackBuild
sed -i "s/<you>/Lockywolf/g" python3-$l_package.SlackBuild
sed -i "s/<where you live>//g" python3-$l_package.SlackBuild

awk '/# \|-----------------------------------------------------------------\| #/{flag=!flag; next} !flag' python3-$l_package.SlackBuild | sponge python3-$l_package.SlackBuild


sed -i "/PRGNAM=appname/aTARNAM=$l_package" python3-$l_package.SlackBuild
sed -i "s/1\.4\.1}.*/$l_version}/g" python3-$l_package.SlackBuild

sed -i "s/appname			# replace with name of program/python3-$l_package/g"  python3-$l_package.SlackBuild

sed -i 's/}		# the "_SBo" is required/}/g'  python3-$l_package.SlackBuild

sed -i '/# Automatically determine the archite/d'  python3-$l_package.SlackBuild

sed -i '/use uname -m for all other arc/d'  python3-$l_package.SlackBuild

sed -i 's/}		# For consistency.*$/}/g' python3-$l_package.SlackBuild

sed -i 's/}		# Drop the pac.*$/}/g' python3-$l_package.SlackBuild

sed -i 's/rm -rf $PRGNAM-$VERSION/rm -rf $TARNAM-$VERSION/g' python3-$l_package.SlackBuild
sed -i 's|tar xvf $CWD/$PRGNAM-$VERSION\.tar\.gz|tar xvf $CWD/$TARNAM-$VERSION.tar.gz|g' python3-$l_package.SlackBuild
sed -i 's/cd $PRGNAM-$VERSION/cd $TARNAM-$VERSION/g' python3-$l_package.SlackBuild

sed -i '/Your application may need diffe/d'  python3-$l_package.SlackBuild

sed -i '/ur application uses set/d'  python3-$l_package.SlackBuild

sed -i '/python2/d'  python3-$l_package.SlackBuild

sed -i '/### For python3/d'  python3-$l_package.SlackBuild

sed -i '/If your application only has a pyproject/,/python3 -m installer/d'   python3-$l_package.SlackBuild

sed -i '/ Strip binaries and librarie/d'   python3-$l_package.SlackBuild

sed -i '/Compress man pages/,/move them manually/d' python3-$l_package.SlackBuild

sed -i '/Copy program documentation into the pack/,/ include the SlackBuild script/d' python3-$l_package.SlackBuild

sed -i 's/<documentation>/ ; echo manual check ; exit 2 ;/g' python3-$l_package.SlackBuild

sed -i '/Copy the slack-desc/d' python3-$l_package.SlackBuild

sed -i '/doinst\.sh/d' python3-$l_package.SlackBuild

sed -i '/Make the package/,/makepkg command below/d' python3-$l_package.SlackBuild

printf "Edited slackbuid\n"

cp /home/lockywolf/OfficialRepos/slackbuilds-templates/slack-desc ./

sed -i "s/appname/python3-$l_package/g" slack-desc

firefox https://pypi.org/project/$l_package
