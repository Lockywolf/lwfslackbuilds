#!/usr/bin/chibi-scheme -rpublicmain
(import (chibi))
(import (chibi modules))
(import (meta))

(define-library (sbo)
  (export privatemain)
  (import
    (chibi filesystem)
    (chibi process)
    (chibi io)
    (except (scheme small) file-exists?))
  (begin
    (define (privatemain args)
      #;(display (directory-fold "."
                               (lambda (fname prev)
                                 (cons fname prev)) '()))
      #;(newline)
      (display
       (call-with-process-io
        "ls"
        (lambda (pid in out err)
          (close-output-port in)
          (let ((res (port->bytevector out)))
            (waitpid pid 0)
            (close-input-port out)
            (close-input-port err)
            (call-with-process-io
             "grep -F bash"
             (lambda (pid in out err)
               (write-bytevector res in)
               (close-output-port in)
               (let ((res (port->string out)))
                 (close-input-port out)
                 (close-input-port err)
                 res))))))))))

(define (publicmain . args)
  (apply (module-ref (analyze-module '(sbo)) 'privatemain) args)
  )
