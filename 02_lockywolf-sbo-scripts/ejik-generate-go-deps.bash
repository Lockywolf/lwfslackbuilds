#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2024-03-20 13:32:10 lockywolf>
#+title: Generate Golang deps.
#+author: lockywolf
#+date:
#+created: <2024-03-20 Wed 11:17:00>
#+refiled:
#+language: bash
#+category: slackware
#+tags: slackware, sbo, go, slackbuilds
#+creator: Emacs-30.0.50/org-mode-9.7-pre

cd $(pwd)

if [ ! -f modules.txt ]; then
  echo "modules.txt doesn't exist!"
  exit 1
fi

rm -f src.txt
rm -f md5+src.txt

set -e

grep "^# " modules.txt | while read -r line; do
  pkg=$(echo "$line" | cut -d' ' -f2)
  pkg=$(curl -s https://pkg.go.dev/$pkg | grep -A10 Repository \
    | grep -m1 -o "<a href=\"[^\"]*\"")
  pkg=${pkg:8}
  pkg=${pkg//\"}
  pkg=${pkg/cs.opensource.google\/go\/x/github.com\/golang}

  ver=$(echo "$line" | cut -d' ' -f3)
  ver=${ver%+incompatible}
  ver=${ver%-pre}
  ver7=$ver
  if [[ -z "${ver##*-*}" ]]; then
    ver=${ver##*-}
    #ver7=${ver:0:7}
    ver7=${ver}
  fi

  prg=$(echo $pkg | rev | cut -d/ -f1 | rev)

  case $pkg in
    https://github.com/*)
      src="$pkg/archive/$ver7/$prg-${ver#v}.tar.gz"
      ;;
    https://salsa.debian.org/*)
      src="$pkg/-/archive/$ver/$prg-$ver.tar.gz"
      ;;
    https://gitlab.com/*)
      # TODO: this has problem with # gitlab.com/yawning/utls.git v0.0.12-1
      # https://gitlab.com/yawning/utls/-/archive/v0.0.12-1/utls-0.0.12-1.tar.gz
      src="$pkg/-/archive/$ver/$prg-$ver.tar.gz"
      ;;
    https://gitlab.torproject.org/*)
      src="$pkg/-/archive/$ver/$prg-$ver.tar.gz"
      ;;
    *)
      printf "Error, %s not prepared for build.\n" "$pkg"
      exit 1
  esac
  echo "$src \\" >> src.txt
  (
    rm -rf lwf_tmp
    mkdir lwf_tmp
    cd lwf_tmp
    if ! curl --fail-with-body --output file -s "$src" ; then
      echo "Url is broken: " "$src"
      touch file
    fi
    md=$(md5sum file)
    echo "$src $md" >> ../md5+src.txt
  )
done

truncate --size=-3 src.txt
