#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2023-09-21 13:37:09 lockywolf>
#+title: 
#+author: 
#+date: 
#+created:
#+refiled:
#+language: bash
#+category: 
#+tags: 
#+creator: Emacs-30.0.50/org-mode-9.6.8Emacs GNU Emacs 29.0.50 (build 1, x86_64-slackware-linux-gnu, GTK+ Version 3.24.31, cairo version 1.16.0) of 2022-02-23


#declare -a urls
#declare -a hashes
urls=()
hashes=()
shopt -s lastpipe

mkdir -p vendor-crates

grep -h -A 3 "\[\[package\]\]" $(find ./ -maxdepth 1 -mindepth 1 -name Cargo.lock | tr '\n' ' ') | \
    sed 's/[[:space:]]*=[[:space:]]*/=/g;s/^--//;s/^\[\[/--\n[[/' | \
    awk 'BEGIN { RS = "--\n" ; FS="\n" } { print $2, $3, $4 }' | sed 's/"//g;s/name=//;s/ version=/=/' | \
    grep crates\.io-index | sed 's/ source=.*$//' | sort -u | while read -r dep ; do

    ver="$(printf "%s\n" "$dep" | cut -d= -f2)"
    dep="$(printf "%s\n" "$dep" | cut -d= -f1)"
    # printf "dep=%s , ver=%s\n" "$dep" "$ver"
    url=https://static.crates.io/crates/$dep/$dep-$ver.crate
    file=$dep-$ver.crate
    urls+=("$url")
    (
      cd vendor-crates
      wget -c "$url"
    )
    hash=$(md5sum vendor-crates/"$file" | awk '{print($1);}')
    hashes+=("$hash")
done
printf "after loop\n"

for i in "${!urls[@]}" ; do
  printf "%s %s\n" "${urls[$i]}" '\'
done

for i in "${!hashes[@]}" ; do
  printf "%s %s\n" "${hashes[$i]}" '\'
done
