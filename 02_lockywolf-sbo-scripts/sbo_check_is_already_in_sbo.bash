#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2022-06-02 21:05:17 lockywolf>
#+title: Low quality. Find slackbuilds that are already on sbo.
#+author: lockywolf
#+date: 
#+created: <2022-04-27 Wed>
#+refiled:
#+language: bash
#+category: slackbuilds
#+tags: slackware, admin, slackbuilds,
#+creator: Emacs GNU Emacs 29.0.50 (build 1, x86_64-slackware-linux-gnu, GTK+ Version 3.24.31, cairo version 1.16.0) of 2022-02-23


readarray -t packages < <(nice -n -20 find /var/lib/sbopkg/SBo/15.0/  -type d -maxdepth 2 -mindepth 2  -not -path '.'  | awk --field-separator='/' '{print $8;}' | sort )

readarray -t mysbo < <(find . -maxdepth 1 -type d -not -path '.' | sed 's/\.\///g'  | sed 's/\.unedited//g' | sort)

for existing in "${packages[@]}"
do
  for mine in "${mysbo[@]}"
  do
    if [[ "$existing" == *"$mine"* ]]
    then
      printf "%s ⊆ %s\n " "$existing" "$mine"
    fi
  done
done

