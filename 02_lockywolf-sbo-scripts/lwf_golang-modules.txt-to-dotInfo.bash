#!/bin/bash
#+created: <2023-11-18 Sat 16:29>

# first generate vendor/modules.txt by running go mod vendor 

l_filepath="$1"

if [[ ! -e "$l_filepath" ]] ; then
  echo "usage: $0 path-to-modules.txt" 1>&2
  exit 1
fi

echo "processing $l_filepath" 1>&2

readarray -t  l_zips < <(grep -e '^# ' "$l_filepath")

# https://github.com/mattn/go-xmpp/archive/a6b124c/go-xmpp-a6b124c9b29d3511e65290b291f75f529f9d50e3.tar.gz

#echo array-size="${#l_zips[@]}"

for pkg in "${l_zips[@]}" ; do
  # echo "$pkg"
  repo=$(echo $pkg | awk '{print $2;}')
  version=$(echo $pkg | awk '{print $3;}')
  echo "repo=$repo" "version=$version" 1>&2

  if [[ "github.com" != "${repo:0:10}" ]] ; then
    echo non github url. not implemented 1>&2
    continue
  else
    echo github url. working 1>&2

    l_server="https://github.com"

    pkg_regex='(.*)/(.*)/(.*)'
    pkg_regex_versioned='(.*)/(.*)/(.*)/v(.*)'
    if [[ "$repo" =~ $pkg_regex_versioned ]] ; then
      echo found versioned project "$l_proj" 1>&2
      l_group="${BASH_REMATCH[2]}"
      l_proj="${BASH_REMATCH[3]}"
    elif [[ "$repo" =~ $pkg_regex ]] ; then
      l_group="${BASH_REMATCH[2]}"
      l_proj="${BASH_REMATCH[3]}"
      echo found normal project "$l_proj" 1>&2
    else
      echo found abnormal project. not implemented 1>&2
      continue
    fi

  fi

  commit_version_regex='(.*)-(.*)-(.*)'
  if [[ "$version" =~ $commit_version_regex ]] ; then
    echo found a shitty version 1>&2
    l_baseversion="${BASH_REMATCH[1]}"
    l_date="${BASH_REMATCH[2]}"
    l_commit="${BASH_REMATCH[3]}"
    echo base=$l_baseversion date=$l_date commit=$l_commit 1>&2
    version="$l_commit"
  else
    echo found normal version 1>&2
  fi

  url="$l_server/$l_group/$l_proj/archive/$version/$l_proj-$version.tar.gz"
  responce=$(curl -s "$url")
  echo "testing $url"
  if [[ "$responce" != "Not Found" ]] ; then
    echo valid url 1>&2
  else
    echo invalid url 1>&2
  fi

  echo 1>&2
  echo 1>&2
done

