#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2022-06-06 11:56:33 lockywolf>
#+title: Utility functions for SlackBuild generation an maintenance.
#+author: lockywolf
#+date: 
#+created: <2022-06-06 Mon>
#+refiled:
#+language: bash
#+category: admin
#+tags: slackware, slackbuilds, bash
#+creator: Emacs GNU Emacs 29.0.50 (build 1, x86_64-slackware-linux-gnu, GTK+ Version 3.24.31, cairo version 1.16.0) of 2022-02-23

#   ln -s ../../lwf_downloads_for_slackbuilds/"$L_FILENAME" .



function lwf_sbo_fetch_source()
(
  L_URL="$1"
  # https://gitlab.gnome.org/GNOME/jsonrpc-glib/-/archive/3.42.0/jsonrpc-glib-3.42.0.tar.gz
  pushd ~/DevLinux/lwf_downloads_for_slackbuilds/ || exit
  wgexec curl --remote-name "$L_URL"
  L_FILENAME=$(find . -maxdepth 1 -type f -printf "%T@ - %p\n" | sort -n | tail -1 | awk '{print $3;}')
  L_FILENAME="${L_FILENAME#./}"
  printf "debug:your filename is %s\n" "$L_FILENAME"
  printf "%s\n" "$L_FILENAME"
  popd || exit
)

function lwf_sbo_detect_commit_or_version()
(
  L_TYPE=""
  L_RE_COMMIT="(.*)-([01234567890abcdef]{40}).*"  # HEURISTIC 001
  L_RE_VERSION="(.*)-([01234567890]+[01234567890.]*).*" # does not support openssl versioning # HEURISTIC 002
  L_ARG1="$1"
  if [[ "$L_ARG1" =~ $L_RE_COMMIT ]]
  then
    L_TYPE=commit
    L_COMMIT="${BASH_REMATCH[2]}"
    echo commit present "$L_COMMIT"
    L_NAME=$(echo "${BASH_REMATCH[1]}" | awk --field-separator='/' '{print $NF;}')
    echo program name detected as "$L_NAME"
  elif [[ "$L_ARG1" =~ $L_RE_VERSION ]]
  then
    L_TYPE=version
    L_VERSION="${BASH_REMATCH[2]}"
    echo version present "$L_VERSION"
    L_NAME=$(echo "${BASH_REMATCH[1]}" | awk --field-separator='/' '{print $NF;}')
    echo program name detected as "$L_NAME"
  else
    echo commit not present
    exit 1
  fi
  printf "%s\n" "$L_TYPE"
  printf "%s\n" "$L_NAME"
  printf "%s%s\n" "$L_COMMIT" "$L_VERSION"
)

function lwf_sbo_make_project_from_url()
(
  echo TODO
  L_FILENAME=$(lwf_sbo_fetch_source "$1" | tail -n 1)
  echo file name is "$L_FILENAME"
  
  
)
